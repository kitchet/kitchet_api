<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Mod_api extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * it will return true when the given id exist on the db
     * @param type $facebook_id
     */
    function check_user_exist($id, $column = 'facebook_id') {
        $this->db->where($column, $id);
        $result = $this->db->get('user')->result();
        if ($result) {
            return $result[0];
        }
        return false;
    }

    function check_menu_exist($id) {
        $this->db->where('id', $id);
        $result = $this->db->get('menu')->result();
        if ($result) {
            return $result[0];
        }
        return false;
    }

    /**
     * insert data to user table it will return a id after successful insert
     * @param type $data
     * @return type
     */
    function add_user($data) {
        $this->db->insert('user', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function add_menu($data) {
        $this->db->insert('menu', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function fetch_all_feed($start = 0, $limit = 10) {

        $this->db->select('menu.id,menu.menu_name,menu.description,menu.photo,menu.time_duration,
                menu.total_item,menu.total_likes,menu.price,menu.delivery_mode,menu.stores,
                menu.tags,menu.date_created,menu.user_id,user.first_name,user.last_name,user.store_name,user.store_tagline,user.facebook_id');
        $this->db->from('menu');
        $this->db->join('user ', 'user.id = menu.user_id');
        $this->db->where('menu.deleted', 0);
        $this->db->order_by("menu.date_created", "desc");
            $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result();
    }

        function search($keyword,$start = 0, $limit = 10) {

        $this->db->select('menu.id,menu.menu_name,menu.description,menu.photo,menu.time_duration,
                menu.total_item,menu.total_likes,menu.price,menu.delivery_mode,menu.stores,
                menu.tags,menu.date_created,menu.user_id,user.first_name,user.last_name,user.store_name,user.store_tagline,user.facebook_id');
        $this->db->from('menu');
        $this->db->join('user ', 'user.id = menu.user_id');
        $this->db->where('menu.deleted', 0);
        $this->db->like('menu.menu_name',$keyword);
        $this->db->or_like('menu.description',$keyword);
        $this->db->or_like('user.store_name',$keyword);
        $this->db->or_like('user.store_tagline',$keyword);
        $this->db->order_by("menu.date_created", "desc");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result();
    }
    
    
    
    function fetch_all_mymenu($user_id, $start = 0, $limit = 10) {

        $this->db->select('menu.id,menu.menu_name,menu.description,menu.photo,menu.time_duration,
                menu.total_item,menu.total_likes,menu.price,menu.delivery_mode,menu.stores,
                menu.tags,menu.date_created,menu.user_id,user.first_name,user.last_name,user.store_name,user.store_tagline,user.facebook_id');
        $this->db->from('menu');
        $this->db->join('user ', 'menu.user_id = user.id');
        $this->db->where('menu.deleted', 0);
        $this->db->where('menu.user_id', $user_id);
        $this->db->order_by("menu.date_created", "desc");
            $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result();
    }

    function favorites($user_id, $start = 0, $limit = 10) {
        $this->db->select('user.id as kitchet_id,user.facebook_id,user.first_name,user.last_name,store_tagline,store_name');
        $this->db->from('favorites');
        $this->db->join('user ', 'user.id = favorites.kitchet_id');
         $this->db->where('favorites.deleted', 0);
         $this->db->where('user_id', $user_id);
          $this->db->order_by("favorites.date_created", "desc");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result();
    }

    function update_fav_kitchet($user_id, $menu_id, $type = '1') {
        $data = array('type_fav' => $type);
        $this->db->where('user_id', $user_id);
        $this->db->where('kitchet_id', $menu_id);
        $this->db->update('favorites', $data);
    }

    function add_to_favorite($user_id, $menu_id) {
        $data = array('user_id' => $user_id, 'kitchet_id' => $menu_id);
        $this->db->insert('favorites', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function check_fav_kitchet($user_id, $menu_id) {
        $this->db->where('user_id', $user_id);
        $this->db->where('kitchet_id', $menu_id);
        $result = $this->db->get('favorites')->result();
        if ($result) {
            return $result[0];
        }
        return false;
    }

    function likes($user_id, $start = 0, $limit = 10) {

        $this->db->select('menu.id as menu_id,menu.menu_name,menu.description,menu.photo,menu.time_duration,
                menu.total_item,menu.total_likes,menu.price,menu.delivery_mode,menu.stores,
                menu.tags,menu.date_created,menu.user_id as owner_id,owner.first_name,owner.last_name,owner.store_name,owner.store_tagline,owner.store_tagline,owner.facebook_id');
        $this->db->from('likes');
        $this->db->join('menu', 'menu.id = likes.menu_id');
        $this->db->join('user owner', 'owner.id = menu.user_id');
        $this->db->where('likes.deleted', 0);
        $this->db->where('likes.user_id', $user_id);
        $this->db->where('likes.type', 1);
        $this->db->order_by("likes.date_created", "desc");
            $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result();
    }

    function update_like_($user_id, $menu_id, $type = '1') {
        $data = array('type' => $type);
        $this->db->where('user_id', $user_id);
        $this->db->where('menu_id', $menu_id);
        $this->db->update('likes', $data);
    }
    
    
    function update_kitchet_info($user_id,$store_name,$store_tagline){
        $data = array('store_name'=>$store_name,
            'store_tagline'=>$store_tagline);
        $this->db->where('user_id', $user_id);
        $this->db->update('user', $data);
    }
    
    function fetch_kitchet_info($user_id){
        $this->db->select('store_name,store_tagline');
          $this->db->from('user');
          $this->db->where('user_id',$user_id);
           $query = $this->db->get();
        return $query->result();
    }

    function like($user_id, $menu_id) {
        $data = array('user_id' => $user_id, 'menu_id' => $menu_id);
        $this->db->insert('likes', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function check_likes($user_id, $menu_id) {
        $this->db->where('user_id', $user_id);
        $this->db->where('menu_id', $menu_id);
        $result = $this->db->get('likes')->result();
        if ($result) {
            return $result[0];
        }
        return false;
    }

    function count_fav($menu_id) {
        $this->db->where('menu_id', $menu_id);
        $this->db->where('type_fav', 1);
        $this->db->from('favorites');
        return $this->db->count_all_results();
    }

    function count_like($menu_id) {
        $this->db->where('menu_id', $menu_id);
        $this->db->where('type', 1);
        $this->db->from('likes');
        return $this->db->count_all_results();
    }

    function update_menu_likes($menu_id) {
        $count = $this->count_like($menu_id);
        $data = array('total_likes' => $count);
        $this->db->where('id', $menu_id);
        $this->db->update('menu', $data);
        return $count;
    }

    function update_menu_item($menu_id, $total_item) {
        $count = $this->count_like($menu_id);
        $data = array('total_item' => $total_item);
        $this->db->where('id', $menu_id);
        $this->db->update('menu', $data);
        return $count;
    }

    function add_to_order($customer_id,$menu_id,$total_item) {
         $data = array('customer_id' => $customer_id, 'menu_id' => $menu_id,'total_item'=>$total_item);
        $this->db->insert('order_food', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    
    function orders($user_id, $start = 0, $limit = 10){
     $this->db->select('menu.id as menu_id,menu.menu_name,menu.description,menu.photo,menu.time_duration,menu.total_item,menu.total_likes,
                order_food.total_item as total_item_ordered,menu.price,menu.delivery_mode,menu.stores,
                menu.tags,menu.date_created,order_food.customer_id,order_food.date_created as date_ordered,user.first_name,user.last_name,user.store_name,user.store_tagline,user.facebook_id');
        $this->db->from('menu');
        $this->db->join('order_food ', 'menu.id = order_food.menu_id');
        $this->db->join('user ', 'user.id = order_food.customer_id');
        $this->db->join('user s2','s2.id=menu.user_id');
        $this->db->where('menu.deleted', 0);
        $this->db->where('menu.id', $user_id);
        $this->db->order_by("menu.date_created", "desc");
            $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result();
    }

}
