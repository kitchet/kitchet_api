<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class API extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mod_api');
        $this->load->helper('url');
    }

    public function index() {
        
    }

    public function login() {
        $data = $this->input->get();
        $facebook_id = isset($data['facebook_id']) ? $data['facebook_id'] : '';
        if ($facebook_id == '') {
            // header('HTTP/1.0 401 Unauthorized');
            $message = "You need to a facebook id to login";
            $this->_message_encoded($message, 0);
        } else {
            $result = $this->mod_api->check_user_exist($facebook_id);
            if (!$result) {
                $this->_message_encoded("not yet register", 0);
            } else {
                $res_arr = array(
                    "response_code" => 1,
                    "message" => "login success",
                    "user" => $result,
                );
                header('Content-Type: application/json');
                echo json_encode($res_arr);
            }
        }
    }

    public function register() {
        $data = $this->input->post();

        $r_data = array();
        $r_data['facebook_id'] = isset($data['facebook_id']) ? $data['facebook_id'] : '';
        $r_data['first_name'] = isset($data['first_name']) ? $data['first_name'] : '';
        $r_data['last_name'] = isset($data['last_name']) ? $data['last_name'] : '';
        $r_data['email_address'] = isset($data['email_address']) ? $data['email_address'] : '';
        $r_data['mobile_num'] = isset($data['mobile_num']) ? $data['mobile_num'] : '';
        $r_data['gender'] = isset($data['gender']) ? $data['gender'] : '';
        $r_data['country'] = isset($data['country']) ? $data['country'] : '';
        $r_data['city'] = isset($data['city']) ? $data['city'] : '';
        $r_data['store_name'] = isset($data['store_name']) ? $data['store_name'] : '';
        $r_data['store_tagline'] = isset($data['store_tagline']) ? $data['store_tagline'] : '';

        $is_field_empty = $this->_check_empty_fields($r_data);
        if ($is_field_empty) {
            return;
        }

        $user_data = $this->_check_user_exist($r_data['facebook_id']);
        if (!$user_data) {
            $id = $this->mod_api->add_user($r_data);
            $r_data['id'] = $id;
            $res_arr = array(
                "response_code" => 1,
                "message" => "register success",
                "user" => $r_data,
            );
        } else {
            $res_arr = array(
                "response_code" => 1,
                "message" => "user already register",
                "user" => $user_data
            );
        }
        header('Content-Type: application/json');
        echo json_encode($res_arr);
    }

    private function _check_user_exist($facebook_id, $column = 'facebook_id') {
        $result = $this->mod_api->check_user_exist($facebook_id, $column);
        return $result;
    }

    private function _message_encoded($message, $status) {
        header('Content-Type: application/json');

        echo json_encode(array("response_code" => $status, "message" => $message));
    }

    public function feeds() {
        $data = $this->input->get();
        $start = isset($data['start']) ? $data['start'] : '';
        $limit = isset($data['limit']) ? $data['limit'] : '';
        $response_data = $this->mod_api->fetch_all_feed($start, $limit);
        if (count($response_data) == 0) {
            $res_arr = array(
                "response_code" => 0,
                "message" => "empty feeds"
            );
        } else {
            $res_arr = array(
                "response_code" => 1,
                "menu" => $response_data
            );
        }


        header('Content-Type: application/json');
        echo json_encode($res_arr);
    }
    
     public function search() {
        $data = $this->input->get();
        $start = isset($data['start']) ? $data['start'] : '';
        $limit = isset($data['limit']) ? $data['limit'] : '';
        $keyword = isset($data['keyword']) ? $data['keyword'] : '';
        $response_data = $this->mod_api->search($keyword,$start, $limit);
        if (count($response_data) == 0) {
            $res_arr = array(
                "response_code" => 0,
                "message" => "empty search result"
            );
        } else {
            $res_arr = array(
                "response_code" => 1,
                "menu" => $response_data
            );
        }


        header('Content-Type: application/json');
        echo json_encode($res_arr);
    }
    
    

    private function _check_empty_fields($r_data) {
        $empty_fields = array();
        foreach ($r_data as $key => $value) {
            if ($value == '') {
                array_push($empty_fields, $key);
            }
        }
        if (count($empty_fields)) {
            $res_arr = array(
                "response_code" => 0,
                "message" => "please fill in the necessary fields",
                "empty_fields" => $empty_fields
            );
            header('Content-Type: application/json');
            echo json_encode($res_arr);
            return true;
        }
    }

    private function _randomName($len = 26) {
        $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $base = strlen($charset);
        $result = '';

        $now = explode(' ', microtime())[1];
        while ($now >= $base) {
            $i = $now % $base;
            $result = $charset[$i] . $result;
            $now /= $base;
        }
        return substr($result, -5);
    }

    public function upload_menu() {
        // Path to move uploaded files
        $target_path = "./images/";
        // array for final json respone
        $response = array();
        // getting server ip address
        $server_ip = gethostbyname(gethostname());
        // final file url that is being uploaded
        $file_upload_url = 'http://' . $server_ip . '/' . 'kitchet_api' . '/' . $target_path;

        $r_data = array();
        if (isset($_FILES['photo']['name'])) {
            $path = $_FILES['photo']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $new_file_name = $this->_randomName();
            //$target_path = $target_path . basename($_FILES['photo']['name']);
            $target_path = $target_path . $new_file_name . "." . $ext;
            // reading other post parameters
            $r_data['menu_name'] = isset($_POST['menu_name']) ? $_POST['menu_name'] : '';
            $r_data['description'] = isset($_POST['description']) ? $_POST['description'] : '';
            $r_data['time_duration'] = isset($_POST['time_duration']) ? $_POST['time_duration'] : '';
            $r_data['total_item'] = isset($_POST['total_item']) ? $_POST['total_item'] : '';
            $r_data['user_id'] = isset($_POST['user_id']) ? $_POST['user_id'] : '';
            $r_data['price'] = isset($_POST['price']) ? $_POST['price'] : '';
            $r_data['delivery_mode'] = isset($_POST['delivery_mode']) ? $_POST['delivery_mode'] : '';
            $r_data['tags'] = isset($_POST['tags']) ? $_POST['tags'] : '';
            $r_data['stores'] = isset($_POST['stores']) ? $_POST['stores'] : '';
            $r_data['photo'] = $new_file_name . "." . $ext;

            try {
                // Throws exception incase file is not being moved
                if (!move_uploaded_file($_FILES['photo']['tmp_name'], $target_path)) {
                    // make error flag true
                    $response['message'] = 'Could not move the file!';
                    $response['response_code'] = '0';
                } else {
                    // File successfully uploaded
                    $response['message'] = 'File uploaded successfully!';
                    $response['response_code'] = '1';
                    $response['file_path'] = $file_upload_url . $new_file_name . "." . $ext;

                    $id = $this->mod_api->add_menu($r_data);
                    $r_data['id'] = $id;
                    $response['data'] = $r_data;
                }
            } catch (Exception $e) {
                // Exception occurred. Make error flag true
                $response['response_code'] = '0';
                $response['message'] = $e->getMessage();
            }
        } else {
            // File parameter is missing
            $response['response_code'] = '0';
            $response['message'] = 'Not received any file!';
        }

// Echo final json response to client
        echo json_encode($response);
    }

    public function favorite() {


        $data = $this->input->get();

        $r_data = array();
        $r_data['user_id'] = isset($data['user_id']) ? $data['user_id'] : '';
        $r_data['kitchet_id'] = isset($data['kitchet_id']) ? $data['kitchet_id'] : '';
        $r_data['type_fav'] = isset($data['type_fav']) ? $data['type_fav'] : '';
        $is_field_empty = $this->_check_empty_fields($r_data);
        if ($is_field_empty) {
            return;
        }
        $isUserExist = $this->mod_api->check_user_exist($r_data['user_id'], 'id');
        if (!$isUserExist) {
            $this->_message_encoded("user not register", 0);
            return;
        }
        $isMenuExist = $this->mod_api->check_user_exist($r_data['kitchet_id'], 'id');
        if (!$isMenuExist) {
            $this->_message_encoded("kitchet doesn't exist", 0);
            return;
        }
        $isfav = $this->mod_api->check_fav_kitchet($r_data['user_id'], $r_data['kitchet_id']);
        if ($isfav) {
            $this->mod_api->update_fav_kitchet($r_data['user_id'], $r_data['kitchet_id'], $r_data['type_fav']);
            $verb = $r_data['type_fav'] == '1' ? 'favorited' : 'unfavorited';
            $this->_message_encoded("you " . $verb . " this kitchet store", 1);
            return;
        }
        $id = $this->mod_api->add_to_favorite($r_data['user_id'], $r_data['kitchet_id']);
        if ($id) {
            $this->_message_encoded("you favorited this kitchet store", 1);
        }
    }

    public function like() {
        $data = $this->input->get();
        $r_data = array();
        $r_data['user_id'] = isset($data['user_id']) ? $data['user_id'] : '';
        $r_data['menu_id'] = isset($data['menu_id']) ? $data['menu_id'] : '';
        $r_data['type'] = isset($data['type']) ? $data['type'] : '';
        $is_field_empty = $this->_check_empty_fields($r_data);
        if ($is_field_empty) {
            return;
        }
        $isUserExist = $this->mod_api->check_user_exist($r_data['user_id'], 'id');
        if (!$isUserExist) {
            $this->_message_encoded("user not register", 0);
            return;
        }
        $isMenuExist = $this->mod_api->check_menu_exist($r_data['menu_id']);
        if (!$isMenuExist) {
            $this->_message_encoded("menu doesn't exist", 0);
            return;
        }
        $isfav = $this->mod_api->check_likes($r_data['user_id'], $r_data['menu_id']);
        if ($isfav) {
            $this->mod_api->update_like_($r_data['user_id'], $r_data['menu_id'], $r_data['type']);
            $verb = $r_data['type'] == '1' ? 'liked' : 'unliked';
            $count = $this->mod_api->update_menu_likes($r_data['menu_id']);

            $res_arr = array(
                "response_code" => 1,
                "message" => "you " . $verb . " this menu",
                "total_likes" => $count
            );
            echo json_encode($res_arr);
            return;
        }
        $id = $this->mod_api->like($r_data['user_id'], $r_data['menu_id']);
        if ($id) {
            $count = $this->mod_api->update_menu_likes($r_data['menu_id']);

            $res_arr = array(
                "response_code" => 1,
                "message" => "you liked this menu",
                "total_likes" => $count
            );
            echo json_encode($res_arr);
        }
    }

    public function likes() {
        $data = $this->input->get();
        $start = isset($data['start']) ? $data['start'] : '';
        $limit = isset($data['limit']) ? $data['limit'] : '';
        $r_data = array();
        $r_data['user_id'] = isset($data['user_id']) ? $data['user_id'] : '';
        $is_field_empty = $this->_check_empty_fields($r_data);
        if ($is_field_empty) {
            return;
        }
        $isUserExist = $this->mod_api->check_user_exist($r_data['user_id'], 'id');
        if (!$isUserExist) {
            $this->_message_encoded("user not register", 0);
            return;
        }
        $response_data = $this->mod_api->likes($r_data['user_id'], $start, $limit);
        if (count($response_data) == 0) {
            $res_arr = array(
                "response_code" => 0,
                "message" => "empty likes"
            );
        } else {
            $res_arr = array(
                "response_code" => 1,
                "menu" => $response_data
            );
        }


        header('Content-Type: application/json');
        echo json_encode($res_arr);
    }

    public function favorites() {
        $data = $this->input->get();
        $start = isset($data['start']) ? $data['start'] : '';
        $limit = isset($data['limit']) ? $data['limit'] : '';

        $r_data = array();
        $r_data['user_id'] = isset($data['user_id']) ? $data['user_id'] : '';
        $is_field_empty = $this->_check_empty_fields($r_data);
        if ($is_field_empty) {
            return;
        }
        $isUserExist = $this->mod_api->check_user_exist($r_data['user_id'], 'id');
        if (!$isUserExist) {
            $this->_message_encoded("user not register", 0);
            return;
        }
        $response_data = $this->mod_api->favorites($r_data['user_id'], $start, $limit);
        if (count($response_data) == 0) {
            $res_arr = array(
                "response_code" => 0,
                "message" => "empty likes"
            );
        } else {
            $res_arr = array(
                "response_code" => 1,
                "menu" => $response_data
            );
        }


        header('Content-Type: application/json');
        echo json_encode($res_arr);
    }

    public function kitchet() {
        $data = $this->input->get();
        $start = isset($data['start']) ? $data['start'] : '';
        $limit = isset($data['limit']) ? $data['limit'] : '';
        $r_data = array();
        $r_data['user_id'] = isset($data['user_id']) ? $data['user_id'] : '';
        $is_field_empty = $this->_check_empty_fields($r_data);
        if ($is_field_empty) {
            return;
        }
        $isUserExist = $this->mod_api->check_user_exist($r_data['user_id'], 'id');
        if (!$isUserExist) {
            $this->_message_encoded("user not register", 0);
            return;
        }
        $response_data = $this->mod_api->fetch_all_mymenu($r_data['user_id'], $start, $limit);
        if (count($response_data) == 0) {
            $res_arr = array(
                "response_code" => 0,
                "message" => "empty feeds"
            );
        } else {
            $res_arr = array(
                "response_code" => 1,
                "menu" => $response_data
            );
        }


        header('Content-Type: application/json');
        echo json_encode($res_arr);
    }

    public function order() {
        $r_data = array();
        $r_data['user_id'] = isset($_POST['user_id']) ? $_POST['user_id'] : '';
        $r_data['menu_id'] = isset($_POST['menu_id']) ? $_POST['menu_id'] : '';
        $r_data['total_item'] = isset($_POST['total_item']) ? $_POST['total_item'] : '';
        $isUserExist = $this->mod_api->check_user_exist($r_data['user_id'], 'id');
        if (!$isUserExist) {
            $this->_message_encoded("user not register", 0);
            return;
        }
        $menuItem = $this->mod_api->check_menu_exist($r_data['menu_id']);
        if (!$menuItem) {
            $this->_message_encoded("menu doesn't exist", 0);
            return;
        } else {

            if ($r_data['total_item'] > $menuItem->total_item) {
                $this->_message_encoded("menu can't supply your order", 0);
                return;
            }
            $item_left = $menuItem->total_item - $r_data['total_item'];
            $this->mod_api->update_menu_item($r_data['menu_id'], $item_left);
            $r_data['id'] = $this->mod_api->add_to_order($r_data['user_id'], $r_data['menu_id'], $r_data['total_item']);
            $res_arr = array(
                "response_code" => 1,
                "message" => "ordered successfully",
                "order_menu" => $r_data
            );
            echo json_encode($res_arr);
        }
    }

    public function orders() {
        $data = $this->input->get();
        $start = isset($data['start']) ? $data['start'] : '';
        $limit = isset($data['limit']) ? $data['limit'] : '';
        $r_data = array();
        $r_data['user_id'] = isset($data['user_id']) ? $data['user_id'] : '';
        $is_field_empty = $this->_check_empty_fields($r_data);
        if ($is_field_empty) {
            return;
        }
        $isUserExist = $this->mod_api->check_user_exist($r_data['user_id'], 'id');
        if (!$isUserExist) {
            $this->_message_encoded("user not register", 0);
            return;
        }
        $response_data = $this->mod_api->orders($r_data['user_id'], $start, $limit);
        if (count($response_data) == 0) {
            $res_arr = array(
                "response_code" => 0,
                "message" => "empty orders"
            );
        } else {
            $res_arr = array(
                "response_code" => 1,
                "menu" => $response_data
            );
        }


        header('Content-Type: application/json');
        echo json_encode($res_arr);
    }

    public function update_kitchet() {
        $data = $this->input->post();

        $r_data = array();
        $r_data['user_id'] = isset($data['user_id']) ? $data['user_id'] : '';
        $r_data['store_name'] = isset($data['store_name']) ? $data['store_name'] : '';
        $r_data['store_tagline'] = isset($data['store_tagline']) ? $data['store_tagline'] : '';
        $isUserExist = $this->mod_api->check_user_exist($r_data['user_id'], 'id');
        if (!$isUserExist) {
            $this->_message_encoded("user not register", 0);
            return;
        }
        
        $this->mod_api->update_kitchet_info($r_data['user_id'],$r_data['store_name'],$r_data['store_tagline']);
         $this->_message_encoded("updated successfully", 1);
    }
    
    public function get_kitchet_info(){
        
    }

}
